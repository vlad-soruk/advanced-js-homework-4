let starWarsUrl = "https://ajax.test-danit.com/api/swapi/films";

let listOfFilms = document.querySelector('.films');

function fetchData(link){
    return fetch(link).then(res => res.json())
}

let a = fetchData(starWarsUrl)
    .then(data => data.forEach(film => {
        let listItem = document.createElement('li');
        let charactersDiv = document.createElement('div');
        listItem.append(charactersDiv);

        let filmName = film.name;
        let filmEpisode = film.episodeId;
        let filmOpeningCrawl = film.openingCrawl;

        let charactersArray = [];

        film.characters.forEach(link => {
            fetchData(link).then(data => {
                charactersArray.push(data.name);
                console.log(charactersArray);
                charactersDiv.textContent = `
                Film episode: "${filmEpisode}"; 
                film name: "${filmName}"; 
                film opening crawl: ${filmOpeningCrawl}; 
                film characters: ${charactersArray.join(', ')}`;
                listOfFilms.append(listItem)
            })
        })
    }))